package com.techgatha.loan.LoanProposalSpringBootProject;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;

import com.techgatha.loan.LoanProposalSpringBootProject.constants.LoanConstants;
import com.techgatha.loan.LoanProposalSpringBootProject.exception.EntityNotFoundException;
import com.techgatha.loan.LoanProposalSpringBootProject.model.Collateral;
import com.techgatha.loan.LoanProposalSpringBootProject.model.Customer;
import com.techgatha.loan.LoanProposalSpringBootProject.model.Employee;
import com.techgatha.loan.LoanProposalSpringBootProject.model.Loan;
import com.techgatha.loan.LoanProposalSpringBootProject.repository.CollateralRepository;
import com.techgatha.loan.LoanProposalSpringBootProject.repository.CustomerRepository;
import com.techgatha.loan.LoanProposalSpringBootProject.repository.EmployeeRepository;
import com.techgatha.loan.LoanProposalSpringBootProject.repository.LoanRepository;
import com.techgatha.loan.LoanProposalSpringBootProject.springservice.EmployeeService;
import com.techgatha.loan.LoanProposalSpringBootProject.springservice.LoanService;

//https://www.netsurfingzone.com/hibernate/failed-to-lazily-initialize-a-collection-of-role-could-not-initialize-proxy-no-session/
//https://stackoverflow.com/questions/30549489/what-is-this-spring-jpa-open-in-view-true-property-in-spring-boot/48222934#48222934
//https://www.baeldung.com/spring-open-session-in-view

@SpringBootApplication
public class LoanProposalSpringBootProjectApplication {


	public static void main(String[] args)  {
		
		SpringApplication.run(LoanProposalSpringBootProjectApplication.class, args);
	}
	@Autowired
	private LoanService loanService;

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private EmployeeRepository emprepo;

	@Autowired
	private CollateralRepository collateralRepository;

	@Autowired
	private EmployeeService employeeService;
	@Bean
	public void addRecords() throws Exception 
	{
		Employee e1 = new Employee();
		e1.setEmployeeId("E001");
		e1.setEmployeeName("Joe");
		e1.setPassword("joe-eoo1");

		Employee e2 = new Employee();
		e2.setEmployeeId("E002");
		e2.setEmployeeName("Rose");
		e2.setPassword("rose-eoo2");

		Employee e3 = new Employee();
		e3.setEmployeeId("E003");
		e3.setEmployeeName("Harish");
		e3.setPassword("harish-eoo3");

		emprepo.save(e1);
		emprepo.save(e2);
		emprepo.save(e3);

		Customer c1 = new Customer("Shalini", "Mumbai","shalini@gmail.com",  LoanConstants.ID_TYPE[1], 1200000.00, "shalini", true);
		Customer c2 = new Customer("Kshitij", "Pune","kshitij@gmail.com", LoanConstants.ID_TYPE[0], 56700000.00, "kshitij", false);

		customerRepository.save(c1);
		customerRepository.save(c2);

		int x = 1;
		for(String collateral : LoanConstants.COLLATERAL_TYPE)
		{
			Collateral cl1 = new Collateral("C00"+x++, collateral);
			collateralRepository.save(cl1);
		}
		
		Loan l1 = loanService.applyForLoan(LoanConstants.LOAN_TYPE[0], 2000000, 3, c1.getCustomerEmailId());
		loanService.uploadCollateral(l1.getLoanId(), Arrays.asList("C001"));
		l1.setRemarks("Nothing Set Yet");
		
		Loan l3 = loanService.applyForLoan(LoanConstants.LOAN_TYPE[1], 1500000, 3, c1.getCustomerEmailId());
		loanService.uploadCollateral(l3.getLoanId(), Arrays.asList("C003"));
		l3.setRemarks("Nothing Set Yet");
		
		Loan l2 = loanService.applyForLoan(LoanConstants.LOAN_TYPE[0], 400000000, 3, c2.getCustomerEmailId());
		loanService.uploadCollateral(l2.getLoanId(), Arrays.asList("C002"));
		l2.setRemarks("Nothing Set Yet");
		
		employeeService.approveLoan(l1.getEmployee().getEmployeeId());
		employeeService.approveLoan(l2.getEmployee().getEmployeeId());
		employeeService.approveLoan(l3.getEmployee().getEmployeeId());
		
	}

}
