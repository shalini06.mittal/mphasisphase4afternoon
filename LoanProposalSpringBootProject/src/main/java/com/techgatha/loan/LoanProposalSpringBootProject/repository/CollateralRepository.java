package com.techgatha.loan.LoanProposalSpringBootProject.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.techgatha.loan.LoanProposalSpringBootProject.model.Collateral;
import com.techgatha.loan.LoanProposalSpringBootProject.model.Customer;
import com.techgatha.loan.LoanProposalSpringBootProject.model.Employee;

@Repository
public interface CollateralRepository extends CrudRepository<Collateral	,String>{

}
