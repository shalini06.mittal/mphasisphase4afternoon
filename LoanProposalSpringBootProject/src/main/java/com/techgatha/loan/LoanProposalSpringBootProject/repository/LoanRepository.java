package com.techgatha.loan.LoanProposalSpringBootProject.repository;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.techgatha.loan.LoanProposalSpringBootProject.model.Loan;

@Repository
public interface LoanRepository extends CrudRepository<Loan,String>{

	public ArrayList<Loan> findByEmployeeEmployeeId(String empid);
	
    public Optional<Loan> findByLoanId(String loanid);
    public ArrayList<Loan> findByCustomerCustomerEmailId(String emailid);
	
}
