package com.techgatha.loan.LoanProposalSpringBootProject.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

import com.techgatha.loan.LoanProposalSpringBootProject.constants.LoanConstants;

public class ResponseLoan implements Serializable{

	private String loanId;
	private String loanType;
	private double loanAmount;
	private double interestRate;
	private double period;
	
	private String employeeId;
	private List<Collateral> collaterals;
	private boolean approved = false;
	private String remarks;

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean isApproved) {
		this.approved = isApproved;
	}

	public void setLoanId(String loanId) {
		this.loanId = loanId;
	}

	public String getLoanId() {
		return loanId;
	}


	public String getLoanType() {
		return loanType;
	}

	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}
	public double getLoanAmount() {
		return loanAmount;
	}
	public void setLoanAmount(double loanAmount) {
		this.loanAmount = loanAmount;
	}

	public double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}

	public double getPeriod() {
		return period;
	}

	public void setPeriod(double period) {
		this.period = period;
	}


	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public List<Collateral> getCollaterals() {
		return collaterals;
	}


	public void setCollaterals(List<Collateral> collaterals) {
		this.collaterals = collaterals;
	}

	@Override
	public String toString() {
		return "ResponseLoan [loanId=" + loanId + ", loanType=" + loanType + ", loanAmount=" + loanAmount
				+ ", interestRate=" + interestRate + ", period=" + period + ", employeeId=" + employeeId
				+ ", collaterals=" + collaterals + ", approved=" + approved + ", remarks=" + remarks + "]";
	}


	

}
