<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c'%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src='https://kit.fontawesome.com/a076d05399.js'></script>
<style>
.bg1 {
	background-color: #62316a !important;
}

.bg1 a {
	color: white !important;
}

.loans {
	padding: 30px;
	background-color: #c2b0c4;
	margin: 20px;
}
.profile{
	margin:20px;
	
	padding:20px;
}
</style>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light bg1">
		<div class="container-fluid">
			<a class="navbar-brand" href="#">ABKB Bank</a>
			<button class="navbar-toggler" type="button"
				data-bs-toggle="collapse" data-bs-target="#navbarNav"
				aria-controls="navbarNav" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<li class="nav-item"><a class="nav-link active"
						aria-current="page" href="#">Welcome <c:out
								value="${sessionScope.custemail }"></c:out></a></li>
					<li class="nav-item"><a class="nav-link" href="/customers">DashBoard</a>
					</li>
					<li class="nav-item"><a class="nav-link" href="/customers/profile">Profile</a>
					</li>
					<li class="nav-item"><a class="nav-link" href="/logout">Logout</a>
					</li>


				</ul>
			</div>
		</div>
	</nav>


	<c:if test="${sessionScope.custemail == null }">
		<c:redirect url="/login"></c:redirect>
	</c:if>
	<div class='container profile'>
		
				<div class='row'>
					<div class='col-md-3'>Name:</div>
					<div class='col-md-4'>${sessionScope.cust.customerName }</div>
				</div>
				<div class='row'>
					<div class='col-md-3'>Address</div>
					<div class='col-md-4'>${sessionScope.cust.customerAddress }</div>
				</div>
				<div class='row'>
					<div class='col-md-3'>Identity Type:</div>
					<div class='col-md-4'>${sessionScope.cust.customerIdentity }</div>
				</div>
				<div class='row'>
					<div class='col-md-3'>Annual Income:</div>
					<div class='col-md-4'>${sessionScope.cust.annualIncome }</div>
				</div>
				<div class='row'>
					<div class='col-md-3'>Income Tax Returns Attached:</div>
					<c:if test="${sessionScope.cust.incomeTaxReturnAttached }">
						<div class='col-md-4'>Please Attach Income Tax</div>
					</c:if>
					
					<c:if test="${! sessionScope.cust.incomeTaxReturnAttached }">
						<div class='col-md-4'>Income Tax Attached</div>
					</c:if>
					
				</div>
				<div class='row'>
					<div class='col-md-12'><a href='/customers/profile/edit'>Edit</a></div>
					
				</div>
	</div>

</body>
</html>
