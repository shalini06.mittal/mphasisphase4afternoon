import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-lifecycle',
  templateUrl: './lifecycle.component.html',
  styleUrls: ['./lifecycle.component.css']
})
export class LifecycleComponent implements OnInit , OnChanges, OnDestroy{

  // pass data from the parent component to the child component => @Input
  // child can raise events to the parent component
  @Input()
  ename:string='';

  city:string ='Mumbai';
  constructor() {
    console.log('lifecycle constructor ',this.ename, this.city);
   }
  ngOnDestroy(): void {
   console.log('lifecycle destroy',this.ename, this.city);
  }

  // that listens for changes in property decorated with @Input
  ngOnChanges(changes: SimpleChanges): void {
   console.log('lifecycle on changes',this.ename, this.city);
  }

  ngOnInit(): void {
    console.log('lifecycle ng on init', this.ename, this.city)
  }

  
}
