import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ResponseLoan } from '../model/loan';
import { LoginService } from '../service/login.service';

@Component({
  selector: 'app-loandetails',
  templateUrl: './loandetails.component.html',
  styleUrls: ['./loandetails.component.css']
})
export class LoandetailsComponent implements OnInit {

  loanid:string;
  loan:ResponseLoan
  constructor(private route:ActivatedRoute,
    private ls:LoginService) {
    this.loanid = '';
    this.loan = {};
   }

  ngOnInit(): void {
    this.route.params.subscribe(response=>{
      console.log(response['loanid'])
      this.loanid = response['loanid'];
      this.ls.fetchLoan(this.loanid)
      .subscribe(resp=>{
          this.loan = resp;
      })
    });
  }

}
