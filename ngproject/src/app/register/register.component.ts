import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Customer } from '../model/customer';
import { identity } from '../model/data';
import { RegisterService } from '../service/register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  identity:string[];
  customer:Customer;
  error:string;
  constructor(private rs :RegisterService, private router:Router) { 
    this.identity= identity;
    this.customer=new Customer('','','',0.0,'',false); 
    this.error='';
  }

  ngOnInit(): void {
  }

  register()
  {
    this.rs.registerCustomer(this.customer)
    .subscribe(response=>{
      if(response.status===201)
        this.router.navigate(['/login']);
        else
        this.error='Registration Failed'
    })
  }
  canDeactivate()
  {
    if(this.customer.customerEmailId === '')
      return false;
    return true;
  }
}
