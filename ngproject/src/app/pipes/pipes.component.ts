import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pipes',
  templateUrl: './pipes.component.html',
  styleUrls: ['./pipes.component.css']
})
export class PipesComponent implements OnInit {

  // pipes => used to display data on the html page in a certain format
  
  ename:string ='Shalini';
  dateofjoining = new Date();
  salary:number = 23423432.324662324;
  address={'city':'Mumbai','country':'India'};
  constructor() { }

  ngOnInit(): void {
  }

}
