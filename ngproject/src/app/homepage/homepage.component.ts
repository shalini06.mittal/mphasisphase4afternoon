import { Component, OnInit } from '@angular/core';
import { loanTypes } from '../model/data';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  loanTypes:string[];
  constructor() { 
    this.loanTypes=loanTypes;
  }

  ngOnInit(): void {
  }

}
