import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Customer } from '../model/customer';
import { ResponsePage } from '../model/responses';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  url:string='http://localhost:8081/admin/'
  constructor(private http
    :HttpClient) { }
  registerCustomer(customer:Customer):Observable<ResponsePage>
  {
    return this.http.post<ResponsePage>(this.url+'customer',customer);
  }
}
