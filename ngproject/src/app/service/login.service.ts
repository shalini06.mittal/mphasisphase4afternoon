import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ResponseLoan } from '../model/loan';

import { ResponsePage } from '../model/responses';
import { User } from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  url:string = 'http://localhost:8081/loan/'
  constructor(private http:HttpClient) { 
    console.log('login service')
  }

  loginUser(user:User):Observable<ResponsePage>
  {
    return this.http.post<ResponsePage>
    (this.url+'login', user);
  }
  fetchLoans(email:any):Observable<ResponseLoan[]>
  {
    return this.http.get<ResponseLoan[]>(this.url+'fetch/'+email);
  }
  fetchLoan(loanid:string):Observable<ResponseLoan>
  {
    return this.http.get<ResponseLoan>(this.url+'/'+loanid);
  }
  getEmail()
  {
    return localStorage.getItem('email');
  }
  isLoggedIn():boolean{
    return !!localStorage.getItem('email');
  }
  logout()
  {
    localStorage.removeItem('email');
  }
}
